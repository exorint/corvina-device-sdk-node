# Corvina simulated device example

This is a monorepo containing the core device client library `@corvina/device-client` and an example `@corvina/device-example`.

Please see the respective README files for more information.

To publish new versions:

```
yarn lerna publish
```
